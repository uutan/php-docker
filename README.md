## Dockerfiles

搭建基础公用环境包，方便使用，支持业务不断的提升而需要一些软件的支持但由于本身不懂或耗时导致不方便安装升级的一种解决方案

## 简介

用 docker 容器服务的方式搭建环境，易于维护、升级。使用前需了解 Docker 的基本概念，常用基本命令。
可以一条条命令执行 docker 命令来构建镜像，容器。这里推荐使用 docker-compose 来管理，执行项目，下面是使用流程。

#### 目录

| 目录                   | 说明                              |
| ---------------------- | --------------------------------- |
| --- app                | 应用目录                          |
| --- --- php            | php 应用运行目录                  |
| --- data               | mongo、 mysql 等数据库文件存储    |
| --- --- backups        | 数据备份目录                      |
| --- docs               | 使用文档                          |
| --- logs               | nginx、 mongo、 mysql、php 等日志 |
| --- sercices           | 服务软件配置包                    |
| --- --- mongo          | ubuntu: 4.2                       |
| --- --- mysql          | ubuntu: 8.latest                  |
| --- --- nginx          | ubuntu: 1.17                      |
| --- --- redis          | ubuntu: 5.latest                  |
| --- --- rabbitmq       | ubuntu: 3.7-management            |
| --- --- php            | contos: 7.3.latest                |
| --- --- elasticsearch  | 7.3.2                             |
| --- --- logstash       | 7.3.2                             |
| --- --- kibana         | 7.3.2                             |
| --- --- kafka(zk)      | latest                            |
| --- --- zabbix(client) | 4.0.1                             |
| --- --- filebeat       | 7.3.2                             |
| --- --- metricbeat     | 7.3.2                             |

--- 组合 nginx + mysql + php + redis
